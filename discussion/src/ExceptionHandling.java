import java.util.Scanner;

public class ExceptionHandling {
    public static void  main(String[] args){
        Scanner input = new Scanner(System.in);
        int num = 0;

        System.out.println("Input a number");
        try{
            num = input.nextInt();
        } catch(Exception e){
            System.out.println("Invalid input, this program only accepts integer inputs.");
            e.printStackTrace();
        }
        System.out.print("The number you entered is: " + num);
    }
}
