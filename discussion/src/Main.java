// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        // Section A

        //for loops

        for(int i=0; i<=10; i++){
            System.out.println("Current Count" + i);
        }
        int[] hundreds = {100, 200, 300, 400, 500};
        for(int i = 0; i < hundreds.length; i++){
            System.out.println(hundreds[i]);
        }
        for(int hundred: hundreds){
            System.out.println("This for loop iterates "+ hundred);
        }

        int x = 0;
        while (x < 10){
            System.out.println("Current count: " + x);
            x++;
        }

        //Do while
        int y = 0;
        do {
            System.out.println("Y value: " + y);
        }
        while( y != 0);

        //Section B - Exception Handling
    }
}