import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int num = 1;
        System.out.println("Input an integer whose factorial will be computed");

        int inputNum = input.nextInt();
       try{
            if(inputNum == 0){
                System.out.println("The factorial of " + inputNum +" is 1" );
            }else if(inputNum < 0){
                System.out.println("Invalid");
            }
            else{
                for (int i = 2; i <= inputNum; i++) {
                    num *= i;
                }
                System.out.println("This is the factorial "+ inputNum+ " is " + num);

            }

        } catch (Exception e){
            System.out.println("Invalid");
            e.printStackTrace();
        }
//       try{
//           int i = 2;
//
//
//               if(inputNum == 0 ){
//                   System.out.println("The factorial of " + inputNum +" is 1" );
//               }else if(inputNum < 0){
//                   System.out.println("Invalid");
//               }else{
//                   while (i<=inputNum){
//                       num *= i;
//                       i++;
//                   }
//                   System.out.println("This is the factorial "+ inputNum+ " is " + num);
//
//               }
//
//
//       } catch (Exception e){
//           System.out.println("Invalid");
//           e.printStackTrace();
//       }

    }
}
